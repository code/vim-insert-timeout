" Set update time to configured variable or default 20 seconds for duration of
" insert
function! insert_timeout#() abort

  " Save current value of 'updatetime' into script variable
  let s:updatetime = &updatetime

  " Set 'updatetime' to configured variable or default
  let &updatetime = get(g:, 'insert_timeout_duration', 20000)

  " Restore 'updatetime' when insert mode ends
  autocmd insert_timeout InsertLeave *
        \ let &updatetime = s:updatetime
        \|autocmd! insert_timeout InsertLeave

  " Stop the insert after 'updatetime' seconds
  autocmd insert_timeout CursorHoldI *
        \ stopinsert
        \|autocmd! insert_timeout CursorHoldI

endfunction
