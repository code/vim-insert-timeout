insert\_suspend\_hlsearch.vim
=============================

This plugin leaves insert mode automatically if there is no activity for a
certain number of seconds, with an `'updatetime'` hook.  This is just a plugin
packaging of Vim tip #1540:

<http://vim.wikia.com/wiki/To_switch_back_to_normal_mode_automatically_after_inaction>

Requires Vim 7.0 or later.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
