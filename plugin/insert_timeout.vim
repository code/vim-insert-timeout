"
" insert_timeout.vim: Leave insert mode automatically if there is no activity
" for a certain number of seconds, with an 'updatetime' hook.  This is just a
" plugin packaging of Vim tip #1540.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_insert_timeout') || &compatible || v:version < 700
  finish
endif
let loaded_insert_timeout = 1

" Set up actions in a group
augroup insert_timeout
  autocmd!
  autocmd InsertEnter *
        \ call insert_timeout#()
augroup END
